// Soal 1
const LuasLingkaran = (r) => {
    let hasil = 3.14 * (r**2);
    return hasil;
}
const KelilingLingkaran = (r) => {
    let hasil = 3.14 * (2*r);
    return hasil
}

console.log(LuasLingkaran(14));
console.log(KelilingLingkaran(17));


// Soal 2
let kalimat = ""

const tambahKata = (kata) => {
    kalimat += `${kata} `
}

tambahKata("saya");
tambahKata("adalah");
tambahKata("seorang");
tambahKata("frontend");
tambahKata("developer");

console.log(kalimat);


// Soal 3
const newFunction = (firstName, lastName)=> {
    return {
      firstName,
      lastName,
      fullName: ()=>{
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName(); 


// Soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation, spell);


// Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)