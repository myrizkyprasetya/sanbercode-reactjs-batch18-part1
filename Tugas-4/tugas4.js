// Soal 1
var angka = 2;
var angka2 = 20;

console.log("LOOPING PERTAMA");    
while (angka<=20){

    console.log(angka+" - I love coding");
    angka+=2;
}
console.log("LOOPING KEDUA");
while (angka2>=2){
    console.log(angka2+" - I will become a frontend developer");
    angka2-=2;
}

console.log("\n");

// Soal 2
for (var nomor=1; nomor<=20; nomor++){
    if((nomor%2)===0){
        console.log(nomor+" - Berkualitas");
    } else if((nomor%3)===0 && (nomor%2)===1){
        console.log(nomor+" - I Love Coding");
    } else {
        console.log(nomor+" - Santai");
    }
}

console.log("\n");

// Soal 3
var hasil="";
for (var i=1;i<=7;i++){
    hasil+="#";
    console.log(hasil);
}

console.log("\n");

// Soal 4
var kalimat="saya sangat senang belajar javascript"
console.log(kalimat.split(" "));

console.log("\n");

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var urutan = daftarBuah.sort();
for(n=0; n<=urutan.length-1; n++){
    console.log(urutan[n]);
}