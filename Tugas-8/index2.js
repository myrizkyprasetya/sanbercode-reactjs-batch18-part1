// Soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

var time = 10000;
var index = 0;

function execute(time){
    readBooksPromise(time, books[index])
    .then(function (sisaWaktu){
        if(sisaWaktu !== 1000){
            index++
            execute(sisaWaktu)
        }
    })
    .catch(function (error) {
        console.log("Kekurangan waktu: ", error);
    });
};

execute(time);