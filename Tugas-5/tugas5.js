// Soal 1
function halo(){
    return "Halo Sanbers!"
}

console.log(halo());

// Soal 2
function kalikan(a,b){
    return a*b
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// Soal 3
function introduce(a, b, c, d){
    return "Nama saya "+ a +", umur saya "+ b +" tahun, alamat saya di "+ c +", dan saya punya hobby yaitu "+ d +"!";
}
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 

// Soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
//Mengolah arrayDaftarPeserta ke object
var objectDaftarPeserta = {
    nama : arrayDaftarPeserta[0],
    jenis_kelamin : arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    tahun_lahir : arrayDaftarPeserta[3]
};
console.log(objectDaftarPeserta);


// Soal 5
var daftarBuah = [
    {nama : "Strawberry", warna : "Merah", ada_bijinya : "Tidak", harga : 9000}, 
    {nama : "Jeruk", warna : "Oranye", ada_bijinya : "Ada", harga : 8000}, 
    {nama : "Semangka", warna : "Hijau & Merah", ada_bijinya : "Ada", harga : 10000}, 
    {nama : "Pisang", warna : "Kuning", ada_bijinya : "Tidak", harga : 5000}
]
console.log(daftarBuah[0]);

// Soal 6
var dataFilm = [];

function tambahDataFilm(a, b, c, d){
    dataFilm.push(
        {nama : a, durasi : b, genre : c, tahun : d}
    )
}

tambahDataFilm("Indonesia Love Story", "160 menit", "Romance", 2019);
tambahDataFilm("Warkop DKI", "90 menit", "Komedi", 2003);
tambahDataFilm("The Dark Knight", "152 menit", "Action", 2008);
tambahDataFilm("The Dark Knight Rises", "165 menit", "Action", 2012);

console.log(dataFilm);