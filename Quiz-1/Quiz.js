// Soal 10
/* var pesertaLomba= [["Budi", "Pria", "172cm"], ["Susi", "Wanita", "162cm"], ["Lala", "Wanita", "155cm"], ["Agung", "Pria", "175cm"]];
var objectPesertaLomba = {
    dataPeserta1 : pesertaLomba[0],
    dataPeserta2 : pesertaLomba[1],
    dataPeserta3 : pesertaLomba[2],
    dataPeserta4 : pesertaLomba[3],
}
console.log(objectPesertaLomba);
console.log(pesertaLomba); */

// Soal 12
// Function Luas Lingkaran
/* function luasLingkaran(jari2){
    var hasil = 3.14*(parseFloat(jari2*jari2));
    return hasil
}

function luasSegitiga(alas,tinggi){
    var hasil = 0.5*parseFloat(alas)*parseFloat(tinggi);
    return hasil
}

function luasPersegi(panjang,lebar){
    return panjang*lebar
}
console.log(luasLingkaran(14.5));
console.log(luasSegitiga(17.5,20.5));
console.log(luasPersegi(2,5)); */

// Soal 13
/* var daftarAlatTulis = ["2. Pensil", "5. Penghapus", "3. Pulpen", "4. Penggaris", "1. Buku"];
var urutan = daftarAlatTulis.sort();
n = 0;
while(n<=urutan.length-1){
    console.log(urutan[n]);
    n++;
} */

// Soal 14
var daftarNama = [];

function tambahNama(nama, jenis_kelamin){
    if (jenis_kelamin==="laki-laki"){
        daftarNama.push(
            {
                nama : nama,
                jenis_kelamin : jenis_kelamin,
                panggilan : "Bapak"
            }
        )
    } else if(jenis_kelamin==="perempuan"){
        daftarNama.push(
            {
                nama : nama,
                jenis_kelamin : jenis_kelamin,
                panggilan : "Ibu"
            }
        )
    }
}
tambahNama("Asep","laki-laki");
tambahNama("Siti","perempuan");
tambahNama("Yeni","perempuan");
tambahNama("Rudi","laki-laki");
tambahNama("Adit","laki-laki");

daftarNama.forEach(function(data){
    console.log(data.panggilan, data.nama);
 })
